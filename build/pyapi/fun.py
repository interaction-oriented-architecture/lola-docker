from copyreg import constructor
import logging
from classes import LolaRequest
import conf as configuration
from typing import List, Dict
from fastapi import HTTPException
import glob
from classes import LolaShell
from pathlib import Path
import json
import os
import random
import string
import shutil

""" 
:param: None
:return: string of random characters with length(12)
:info: Generates random string to identify child processes on the host system
"""
def generateUniqueID(n = 12) -> str:
    chars = string.ascii_lowercase
    random_token: str = ''.join(random.choice(chars) for i in range(n))

    logging.info('[ID = {}] == Session started =='.format(random_token))

    return random_token

""" 
Basic function to test whether LOLA files starts with keyword "PLACE"
"""
def basicLolaFormatCheck(p) -> bool:
    return p[0:5] == 'PLACE'

""" 
Translate API formulas to LOLA formulas
"""
def validateFormulas(formulas: List[str], process_identifier: str) -> bool:

    # API caller specifies a list of formulas that will be executed.
    # We check whether these formulas are valid LoLA formulas.
    for formula in formulas:
        if not isValidFormula(formula):
            logging.error('[ID = {}] Aborted: no valid LOLA formulas'.format(process_identifier))
            cleanIO(process_identifier)
            raise HTTPException(status_code=422, detail=f'No valid LOLA formulas supplied.')

    return True


""" 
This function determines if the given formula is a legal LoLA syntax option.
It cannot determine whether the semantics are correct, this is LoLAs task.
"""
def isValidFormula(formula: string) -> bool:
    if formula == 'EF DEADLOCK' or (formula[0:5] == 'AGEF(' and formula[-1] == ')'):
        return True

    return False

""" 
This function writes the LOLA string to a temporary file
It can later be used to perform the actual LOLA operations.
"""
def generateIOInput(pnet: str, process_identifier: str, formulas: list):

    # Verify if the LOLA file is correct
    if(basicLolaFormatCheck(pnet)):

        # Verify if all formulas are valid
        if(validateFormulas(formulas, process_identifier)):

            # Try IO operations
            try:
                # Directory for files used for input LOLA file
                _in_lola: str = os.path.join(configuration.temp_dir_in, process_identifier, 'input')
                # Directory for files that contain formulas
                _in_form: str = os.path.join(configuration.temp_dir_in, process_identifier, 'formulas')
                # Directory that collects output results, one file for each formula.
                _out_gen: str = os.path.join(configuration.temp_dir_out, process_identifier)
                
                # Create all directories as empty containers.
                for IODirGenerationPath in [_in_lola,_in_form,_out_gen]:
                    Path(IODirGenerationPath).mkdir(parents = True, exist_ok = True)

                # Write LOLA string to .lola file.
                f = open(os.path.join(_in_lola,'{}.lola'.format(process_identifier)),'w')
                f.write(pnet)
                f.close()
                
                # Write each formula to a file so it can be used in the LoLA shell.
                # We do this because the CLI has a limit on in-line input characters.
                for index, formula in enumerate(formulas):
                    f = open(os.path.join(_in_form,'{}_{}.formula'.format(index, process_identifier)),'w')
                    f.write(formula)
                    f.close()

            except Exception as e:
                raise HTTPException(status_code=500, detail=f'IO Error. Please report this to the API administator.')
        else:
            raise HTTPException(status_code=422, detail=f'One or more formulas are invalid.')


    else:
        raise HTTPException(status_code=422, detail=f'No valid LOLA file format supplied. Did you encode the JSON file?')

    return True

""" 
Run LOLA code with ``lola [file] [options]``
LOLA2 is a binary installed on this Python host OS. 
This is the only function that is allowed to execute code on os.system !!
It is run as user pyapi-runner which has limited access outside of its home dir.
"""
def runLolaShell(process_identifier: str, uf: str, lf: str) -> bool:

    # Create new shell specifically for identifier
    lola: LolaShell = LolaShell(process_identifier)

    # Execute an analysis for given formula_id (uf) and actual formula (lf)
    if lola.exec(uf, lf):
        return True

    # If lola shell gave back False, raise HTTP error 
    cleanIO(process_identifier)
    raise HTTPException(status_code=500, detail=f'Unknown LOLA shell error.')

""" 
Collect output dir
"""
def collectOutput(process_identifier: str, request: LolaRequest) -> Dict:

    # List all JSON files in output directory of the process_id
    files_in_output_dir: List[str] = glob.glob(os.path.join(configuration.temp_dir_out, process_identifier, '*.json'))
    paths_in_output_dir: List[str] = glob.glob(os.path.join(configuration.temp_dir_out, process_identifier, '*.path'))

    # If no files are found, rase HTTP error
    if not files_in_output_dir:
        logging.error('[ID = {}] No files are found in the output directory'.format(process_identifier))
        cleanIO(process_identifier)
        raise HTTPException(status_code=500, detail=f'IO Error. Please report this to the API administator.')
    
    # Create empty output, will be populated
    output: Dict = {
        "_DAME_VERSION": "1.0",
        "_DAME_INTERNAL_PROCESS": process_identifier,
        "_DAME_EXTERNAL_CALLERID": request.xID,
        "_DAME_FORMULA_CHAIN": request.formulas,
        "_DAME_LOLA_OUTPUT": []
    }

    # Extract JSON from all files and add it to a Python Dict
    for _out in files_in_output_dir:
        # Read JSON and convert to Python Dict
        data = open(_out)
        data_json: Dict = json.load(data)
        data.close()

        has_error = False if 'analysis' in data_json else True

        # Retrieve formula_id from filename without extension
        # This is the naming convention
        formula_id: str = Path(_out).stem

        # If file exists, add it to the output
        fireSequence: list = []
        loc = os.path.join(configuration.temp_dir_out, process_identifier, '{}.path'.format(formula_id))
        if os.path.isfile(loc):
            data = open(loc, 'r')
            fireSequence = data.readlines()
            data.close()
        
        # For each string in fireSequence split on the dot and keep the first element
        fireSequence = [x.split('.')[0] for x in fireSequence]        

        # Reverse the list
        fireSequence.reverse()

        temp_dict: Dict = {
            "_FORMULA_ID" : formula_id,
            "_HAS_ERROR": has_error,
            "_ANALYSIS_RESULT": data_json['analysis'] if not has_error else None,
            "_COUNTER_PATH": fireSequence
        }

        # Add to output
        output['_DAME_LOLA_OUTPUT'].append(temp_dict)
    
    return output

""" 
This function does a cleanup of all IO actions on the system
"""
def cleanIO(process_identifier) -> bool:

    try: 
        # Remove input dir
        shutil.rmtree(os.path.join(configuration.temp_dir_in, process_identifier))
        # Remove output dir
        shutil.rmtree(os.path.join(configuration.temp_dir_out, process_identifier))
    except Exception:
        logging.warning('[ID = {}] Could not remove all IO files for this session'.format(process_identifier))

    logging.info('[ID = {}] == Session ended =='.format(process_identifier))

    return True