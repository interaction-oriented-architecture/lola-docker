# DAME LoLA

This is the official DAME LoLA project (previous aliases: *lola-docker* and *LORD*).

It consists of a single buildable docker image which contains two projects that work together: FastAPI + LoLA. FastAPI acts as a wrapper for the LoLA framework. It handles REST requests, performs I/O operations and uses the LoLA executable. LoLA is a highly optimized tool built in C. It has an open-source repo from which the latest version can be downloaded, yet is not actively maintained nor does it have version management.

## Running the project
For first time use, make sure you have Docker Engine and Docker compose installed on your system, navigate to the root of this repo and run

```
docker compose up --build
```

This will build the image and run it in a container. Running this for the first time will: download the necessary dependencies, extract the source code, patch the source code, prepare the linux environment, build LoLA and run a Python server. It may take a couple of minutes to complete. 

After installation, use standard Docker commands to control the DAME container. Normal use include:

```
docker compose up
```

and 

```
docker compose stop
```

Understanding the structure of the applications run in the docker is very useful for making changes. You need to have some basic knowledge of `docker compose` and the `Dockerfile`. In `build/Dockerfile` you find each step accompanied by comments to guide you through the process.

## Changing the source code

We do not recommend changing the LoLA 2.0 source code. If you decide to do this, do not forget to compress it to `lola2.tar.gz` file and include it in the build directory.

Changing the Fast API wrapper is encouraged. Please find the source-code under `build/pyapi`.

## Structure of the Fast API wrapper.
The Fast API wrapper is called `pyapi`. The file `main.py` controls the application, which is extremely lightweight. For in-depth documentation on Fast API, [check out this link](https://fastapi.tiangolo.com/).

We serve the Python application using `uvicorn` and expose it on port `:8000` (which Docker maps to `:8090`). See documentation on uvicorn [here](https://www.uvicorn.org/).

Fast API uses function decorators to indicate functions that are exposed to a certain endpoint. These are declared using the `@app.event(...)` nontation above a function. We publish two functions to REST allowing all incoming sources (you could change this to be only your local IP for safety, simply replace the `"*"` with a whitelisted array):

- `pyapi() on "/"` This returns the status of the application, if the app is online it returns JSON string `{ status: 200 }`.
- `analyse() on "/analyse` This complex function takes as a POST body argument the class LolaRequest, as defined in `classes.py`. It then starts the serial logic of handling all `LolaRequest.formulas: str[]` on the Petri net in lola format `LolaRequest.lolafile: str`. It executes the following steps:
    - Reads config
    - Generates a unique ID for the incoming request
    - Prepares I/O (e.g. creating directories for input and output, and extracting it from the request)
    - Runs each formula on the Petri net using the `LolaShell` (which is a wrapper for executing `lola` commands in the CLI using Python).
    - Gather output
    - Clean I/O.
    - Returns value.

Other files have the following functionalities:

- `classes.py` contains the classes used for typing and running shell commands in the container.
- `conf.py` config file, you may adjust these if you want to.
  - `temp_dir_in = "/home/pyapi-runner/tmp_files_in"` Where to store input files (extracted from request) during a request.
  - `temp_dir_out = "/home/pyapi-runner/tmp_files_out"` Where to store output files (generated by LoLA) during a request.
  - `log_file = "/home/pyapi-runner/logs/api.log"` Where to store the log.
  - `auth_file = "/home/pyapi-runner/auth/auth.json"` Where to store the auth tokens. These are not used in the current version. Can be implemented.
- `fun.py` All support functions are defined here. These functions do all the hard work. They are all accompanied by comments if you wish to change logic.
- `validation.py` You can add additional validation for requests here. The current version only supports basic validation, and therefore we leave this file empty for future development.

NOTE: In the build file is a file called `requirements.txt`. This file contains all the dependencies used by Python. If you wish to extend `pyapi`, please add new dependencies in this file and re-build the project.

