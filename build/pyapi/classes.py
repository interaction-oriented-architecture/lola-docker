import logging
from typing import Union
import os
from typing import List, Dict
from xmlrpc.client import Boolean
from pydantic import BaseModel
import conf as configuration

""" 
Class used to validate HTTP request
"""
class LolaRequest(BaseModel):
    xID: str
    formulas: List[str]
    lolafile: str

""" 
Runs LOLA code with ``lola [file] [options]``
LOLA2 is a binary installed on this Python host OS. 
This is the only function that is allowed to execute code on os.system !!
It is run as user pyapi-runner which has limited access outside of its home dir.
"""
class LolaShell:
    def __init__(self, process_identifier) -> None:
        self.process_identifier: str = process_identifier

    def exec(self, formula_id: str, formula: str) -> Boolean:

        _in: str = self.__generateInputFileLocation()
        _fm: str = self.__generateFormulaFileLocation(formula_id)
        _out: str = self.__generateOutputFileLocation(formula_id)

        # Execute on system
        os.system('lola {} -f {} --json={}.json --path={}.path'.format(_in, _fm, _out, _out))
        logging.info('[ID = {}] Running LOLA with formula {}'.format(self.process_identifier,_fm))
        
        return True
    
    # Returns input path in /tmp_files_in/{process_id}/input/{process_id}.lola
    def __generateInputFileLocation(self) -> str:
        return os.path.join(configuration.temp_dir_in, self.process_identifier, 'input', '{}.lola'.format(self.process_identifier))

    # Returns JSON output path in /tmp_files_out/{process_id}/{formula_id}.json
    def __generateOutputFileLocation(self, formula_id) -> str:
        return os.path.join(configuration.temp_dir_out, self.process_identifier, '{}'.format(formula_id))

    # Returns input path in /tmp_files_in/{process_id}/input/{process_id}.lola
    def __generateFormulaFileLocation(self, formula_id) -> str:
        return os.path.join(configuration.temp_dir_in, self.process_identifier, 'formulas', '{}_{}.formula'.format(formula_id, self.process_identifier))
