import logging
from typing import Dict, Union
from classes import LolaRequest
from fastapi import FastAPI, HTTPException, Response
import conf as configuration
import classes as classes
import fun as functions
import json
import os
import random
import string
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

# Make sure we can call this API from anywhere
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

"""
This function only fires once the app starts. It prints a unique ID to the console.
Use this ID to create a connection to this LoLA instance.
It is generated one-time and stored in the /home/auth/auth.json
"""
@app.on_event("startup")
async def startup_event():
    print("============= CONNECTION KEY ============= ")
    auth_key_host = None

    # Does auth.json exist?
    # If not, first time app run
    if not os.path.isfile(configuration.auth_file):

        # Create new dict for auth file, with unique key
        # This key can be shared with anyone that needs to connect to this client.
        # If you have a powerful server, this may even be public.
        auth_key_host = functions.generateUniqueID(n = 32)
        _auth: Dict = {
            "auth_key_host": auth_key_host,
            "auth_clients": []
        }

        # Write to auth.json
        
        with open(configuration.auth_file, 'w') as auth_file:
            json.dump(_auth, auth_file)
    else:
        # If there is an auth file, open it.
        data = open(configuration.auth_file)
        auth_dict: Dict = json.load(data)
        data.close()

        if "auth_key_host" in auth_dict and len(auth_dict["auth_key_host"]) == 32:
            auth_key_host = auth_dict["auth_key_host"]
        else:
            raise Exception("Auth file is corrupt. Please remove the file '{}' and restart the application".format(configuration.auth_file))
    
    # print to console on startup
    print('Use this key to establish a personal key with this LoLA client: {}'.format(auth_key_host))
    

""" 
:param: None
:return: JSON status of endpoint
:info: Used to check whether the endpoint is available. Status returns a HTTP status code.
"""
@app.get("/")
async def pyapi(response: Response) -> Dict:
    response.headers["Access-Control-Allow-Origin"] = "*"
    return {
        "status": 200
        }

"""
Main endpoint on HTTP::/analyse
This acceps a JSON POST body formatted as a LolaRequest
It generates a unique process_identifier (32 rand char) string for this request
All logic and IO naming will follow this ID
"""
@app.post("/analyse")
def handle_request(request: LolaRequest, response: Response) -> Dict:
    response.headers["Access-Control-Allow-Origin"] = "*"

    # Set logging
    logging.basicConfig(filename=configuration.log_file, encoding='utf-8', level=logging.INFO)

    # Get unique ID for each request
    process_identifier: string = functions.generateUniqueID()

    # Write the lola file formatted for JSON to a file
    functions.generateIOInput(request.lolafile, process_identifier, request.formulas)

    # Run lola code for each entry
    for uf, lf in enumerate(request.formulas):

        # Runs ``lola [file] [options]`` on the input for each given formula
        # LOLA cannot run multiple formulas at the same time and thus each
        # formula needs to be run serially.
        # This may take a while...
        functions.runLolaShell(process_identifier, uf, lf)

    # Once all output is generated, collect and combine analysis to a single Dict
    analysis_output_combined: Dict = functions.collectOutput(process_identifier, request)

    # Remove in and output from LOLA home dir
    functions.cleanIO(process_identifier)

    return analysis_output_combined


